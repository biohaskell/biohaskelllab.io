{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

import           Clay                          as C
import           Data.Yaml                     as Y
import           Development.Shake
import           Development.Shake.Command     as Shake
import           Development.Shake.FilePath
import           Development.Shake.Util
import           Lucid
import           RIO
import qualified RIO.Text                      as T
import qualified RIO.Text.Lazy                 as LT
import           Slick

site = "public"

data IndexData = IndexData {
  indexContent :: Text
, indexHeader  :: Text
, indexStyle   :: Text
} deriving (Eq, Show, Generic)

instance FromJSON IndexData where
  parseJSON (Object v) = IndexData
    <$> v .: "content"
    <*> v .: "header"
    <*> v .: "style"

instance ToJSON IndexData where
  toJSON x = Y.object ["content" .= indexContent x, "header" .= indexHeader x, "style" .= indexStyle x]

data Page = Page {
  pageContent :: Text
} deriving (Eq, Show, Generic)

instance FromJSON Page where
  parseJSON (Object v) = Page <$> v .: "content"

instance ToJSON Page where
  toJSON x = Y.object ["content" .= pageContent x]

stackRun :: FilePath -> FilePath -> Action ()
stackRun src out = do
  putNormal $ "Running `stack runhaskell " <> src <> " > " <> out
  Stdout stdout <- Shake.command [] "stack" ["runhaskell", src]
  writeFile' out stdout

genStyleHtml :: [FilePath] -> Html ()
genStyleHtml = mconcat . fmap
  ((\x -> link_ [href_ x, rel_ "stylesheet"]) . T.pack . dropDirectory1)

singleStackRun :: FilePath -> FilePath -> Action ()
singleStackRun folder out = do
  let src = folder </> dropDirectory1 out -<.> ".hs"
  need [src]
  stackRun src out

requireCss :: Action [FilePath]
requireCss = do
  c <- getDirectoryFiles "" ["css/*.hs"]
  let cssO = fmap ((-<.> ".css") . (site </>) . dropDirectory1) c
  need cssO
  return cssO

requireHtml :: Action [FilePath]
requireHtml = do
  h <- getDirectoryFiles "" ["html/*.hs"]
  let htmlO = fmap ((-<.> ".html") . ("templates" </>) . dropDirectory1) h
  need htmlO
  return htmlO

main :: IO ()
main = shakeArgs shakeOptions $ do
  let index = site </> "index.html"
  let cssx  = site </> "*.css"
  let htmlt = "templates" </> "*.html"
  let logo = site </> "logo.png"

  want ["index"]

  phony "clean" $ do
    putNormal $ "Cleaning files in " ++ site
    removeFilesAfter "." [site]

  cssx %> singleStackRun "css"
  htmlt %> singleStackRun "html"
  logo %> flip copyFile' <*> dropDirectory1 

  index %> \out -> do
    let src = dropDirectory1 out -<.> ".md"
    need [logo, src]
    cssO  <- requireCss
    htmlO <- requireHtml

    let s = LT.toStrict $ renderText $ genStyleHtml cssO

    indexT         <- compileTemplate' "templates/index.html"
    headerT        <- compileTemplate' "templates/header.html"

    (page :: Page) <- readFile' src >>= markdownToHTML' . T.pack
    writeFile' out $ T.unpack $ substitute
      indexT
      (toJSON $ IndexData
        { indexContent = pageContent page
        , indexStyle   = s
        , indexHeader  = substitute headerT ()
        }
      )

  phony "index" $ need [index]

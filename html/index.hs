{-# LANGUAGE OverloadedStrings #-}

import Lucid
import RIO.Text.Lazy as LT

main = putStrLn . LT.unpack . renderText $ indexHtml

splashHtml :: Html ()
splashHtml = section_ [id_ "splash"] "{{{content}}}"

indexHtml = html_ $ do
  head_ "{{{style}}}"
  body_ $ "{{{header}}}" <> splashHtml

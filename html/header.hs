{-# LANGUAGE OverloadedStrings #-}

import Lucid
import RIO.Text.Lazy as LT

main = putStrLn . LT.unpack . renderText $ headerHtml

navHtml :: Html ()
navHtml = nav_ $
  ul_ $ do
   li_ $ a_ [href_ "index.html"] "Home"
   li_ $ a_ [href_ "https://gitlab.com/biohaskell"] "Gitlab"
   li_ $ a_ [href_ "posts.html"] "Blog"

logoHtml :: Html ()
logoHtml =  div_ [class_ "logo"] $
  a_ [href_ "index.html", title_ "BioHaskell Home Page"] $
    img_ [src_ "logo.png"]

headerHtml = header_ $ logoHtml <> navHtml

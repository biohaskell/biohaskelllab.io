{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import Clay as C
import Clay.Flexbox as F
import RIO hiding (display, div)

main = putCss $ header ? do
  display C.flex
  div # ".logo" ? do
     background white
     img ? display block
  nav ? do
     background     white
     color          "#008800"
     display        C.flex
     fontSize       (px 14)
     padding        (px 5) (px 0) (px 5) (px 0)
     position       relative
     left           (px 0)
     right          (px 0)
     bottom         (px 0)
     flexDirection  row
     flexBasis      (pc 100)
     justifyContent center
     ul ? do
       display       C.flex
       position      relative
       flexDirection row
       justifyContent spaceBetween
       flexBasis     (pc 30)
